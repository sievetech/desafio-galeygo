# -*- coding: utf-8 -*-
from django.db import models


class Region(models.Model):
    id = models.IntegerField(primary_key=True, db_column='RegionID')
    region_description = models.TextField(u'Descrição da região', db_column='RegionDescription')

    class Meta:
        app_label = 'galeygo_admin'
        db_table = 'Region'
