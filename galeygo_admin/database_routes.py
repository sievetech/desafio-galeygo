# *-* coding: utf-8 *-*
from django.conf import settings

DEFAULT = 'default'


class DbAppRouter(object):

    def db_for_read(self, model, **hints):
        if settings.DATABASES.get(model._meta.app_label):
            return model._meta.app_label
        return DEFAULT

    def db_for_write(self, model, **hints):
        if settings.DATABASES.get(model._meta.app_label):
            return model._meta.app_label
        return DEFAULT

    def allow_relation(self, obj1, obj2, **hints):
        if settings.DATABASES.get(obj1._meta.app_label) or settings.DATABASES.get(obj2._meta.app_label):
            return True
        return None

    def allow_syncdb(self, db, model):
        if db == DEFAULT and model._meta.app_label not in settings.DATABASES:
            return True
        if db != DEFAULT and db == model._meta.app_label:
            return True
        return False

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        if db == DEFAULT and app_label not in settings.DATABASES:
            return True
        if db != DEFAULT and db == app_label:
            return True
        return False
